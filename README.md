# Stateless App

This project contains an Angular 15 web named **stateless-app**.

![app image](app-image.png)

## Run locally

```sh
# Open new terminal and start ng server (live update for coding)
ng serve

# Review application in a browser: http://localhost:4200
```

## Run locally as a Container

```sh
# Build Container using podman
podman build --tag stateless-app:1.0.0 .

# Start Container
podman run --name demo -d -p 81:8080 stateless-app:1.0.0

# Review application in a browser: http://localhost:81
```

## Run on OpenShift

Create application:

```sh
# Create namespace
oc new-project stateless-app

# Create application
oc new-app --name=stateless-app --strategy=docker https://gitlab.com/ansible-app-governance-demo/stateless-app

# Deploy and follow logs until image is created
oc logs bc/stateless-app -f

# Expose service
oc expose svc stateless-app

# Get URL
oc get route stateless-app -o jsonpath='{.status.ingress[0].host}'

# Test in your browser
```

## Build Image

```sh
# Build image
podman build -t stateless-app:1.0.0 .

# Login into quay
podman login quay.io

# Tag and push into quay
podman tag stateless-app:1.0.0 quay.io/demo-applications/stateless-app:1.0.0
podman push quay.io/demo-applications/stateless-app:1.0.0
```
