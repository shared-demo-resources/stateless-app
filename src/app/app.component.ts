import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'stateless-app';
  appVersion = null;
  appCluster = null;
  public newTitleElem: any;
  @ViewChild('terminal', { static: true }) public terminal: any;

  constructor(
    // private config: AppConfigService, 
    private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get('./assets/config/info.json')
      .toPromise()
      .then((info: any) => {
        this.appVersion = info.appVersion
        this.appCluster = info.appCluster
        
      })
      .catch((err: any) => {
        console.error(err);
      });
  }

}


